# Vaporless OpenWhisk Template

Hello! 😎

This template allows you to deploy a [Vapor](https://vapor.codes/) app as a service for the OpenWhisk platform via [Vaporless](https://gitlab.com/bensyverson/vaporless). You can create a new project from this template using [Serverless](https://serverless.com/):

```
$ serverless create \
--template-url https://gitlab.com/bensyverson/vaporless-openwhisk \
--path MyApp
```

To connect this template to your Vapor app, follow the steps below:

### 1. 🗝 Set up your account credentials

Before you can deploy your service to OpenWhisk, you need to have an account registered with the platform.

- *Want to run the platform locally?* Please read the project's [*Quick Start*](https://github.com/openwhisk/openwhisk#quick-start) guide for deploying it locally.
- *Want to use a hosted provider?* Please sign up for an account with [IBM Bluemix](https://console.ng.bluemix.net/) and then follow the instructions for getting access to [OpenWhisk on Bluemix](https://console.ng.bluemix.net/openwhisk/). 

Also check out the Serverless [OpenWhisk QuickStart](https://serverless.com/framework/docs/providers/openwhisk/guide/credentials/).

tl;dr? Here's a cheat sheet:

```
$ curl -sL https://ibm.biz/idt-installer | bash
$ ibmcloud login
$ ibmcloud target --cf
```

### 2. 🔌 Install the provider plugin

Install project dependencies which includes the OpenWhisk provider plugin.

```
$ npm install
```

### 3. 🐳 Install the Swift Docker image

Install the Docker image needed to build your project: [`openwhisk/action-swift-v4.2`](https://hub.docker.com/r/openwhisk/action-swift-v4.2).

```
$ docker pull openwhisk/action-swift-v4.2
```

### 4. 📚 Update Package.swift *in your Vapor app*

This version of the template **imports your existing Vapor app as a dependency.** If you don't already have a Vapor app, [install Vapor](https://vapor.codes/) and create one:

```
$ vapor new MyProject
```

You will need to modify the Vapor app's Package.swift to **export your app as a library.** For example:

```swift
let package = Package(
    name: "myproject",
    products: [                                         // 👈 Library
        .library(name: "MyProject", targets: ["App"]),  // 👈 Added
    ],                                                  // 👈 Here
    dependencies: [
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.0"),
    ],
    targets: [
        .target(name: "App", dependencies: ["Vapor"]),
        .target(name: "Run", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App"])
    ]
)
```


### 5. 🏷 Tag your Vapor app

Commit your changes to Package.swift in your Vapor app, push, and then [tag your release](https://help.github.com/en/articles/creating-releases) using the format `1.2.3`. You'll use this tag in the next step.

### 6. 📝 Update Package.swift in this directory

Update the [Package.swift](Package.swift) in this directory to import your Vapor project as a dependency:

```swift
.package(url: "https://github.com/me/my-vapor-app.git", from: "1.0.0"),
```

Be sure to use the right tag from your Vapor app's repo! You'll also need to update the name of the dependency with the name of the Library you defined above:

```swift
dependencies: ["MyProject", "Vaporless"],
```

### 7. 🛠 Build this project locally

Ensure this project is building locally:

```
$ swift build
```

### 8. 👋 Deploy Service

If everything builds fine, use the `serverless` command to deploy your Vapor app to OpenWhisk:

```shell
serverless deploy
```

Note: This command will cross-compile your project, so it may take several minutes.


### 🤷‍♂️ Issues / Feedback / Feature Requests?

If you have any issues, comments or want to see new features, please file an issue in the project repository:

https://gitlab.com/bensyverson/vaporless-openwhisk