// swift-tools-version:4.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Action",
    products: [
        .executable(
            name: "Action",
            targets:  ["Action"]
        )
    ],
    dependencies: [
        .package(url: "https://gitlab.com/bensyverson/vaporless.git", from: "0.1.5"),
        /// Replace this with your own Vapor app:
        .package(url: "https://gitlab.com/bensyverson/vaporhello.git", from: "1.0.1"),
    ],
    targets: [
        .target(
            name: "Action",
            /// Replace `HelloVapor` with the name of your library below:
            dependencies: ["HelloVapor", "Vaporless"],
            path: "."
        )
    ]
)
