import XCTest

import VaporlessHelloTests

var tests = [XCTestCaseEntry]()
tests += VaporlessHelloTests.allTests()
XCTMain(tests)