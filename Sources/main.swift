import Foundation
import App // this is your Vapor project.
import Vaporless

let vaporless = Vaporless(app: try app(.detect()))

func main(args: [String:Any]) -> [String:Any] {
	return vaporless.handle(args: args)
}
